FROM ubuntu:18.04

# Install pre-requisites
RUN sed 's/main$/main universe/' -i /etc/apt/sources.list && \
    apt-get update && apt-get upgrade -y && apt-get install -y software-properties-common && \
    apt-get install -y build-essential openjdk-8-jre openjdk-8-jdk && \
    apt-get install -y libxext-dev libxrender-dev libxtst-dev && \
    apt-get install -y git wget unzip && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /tmp/*

# Install libgtk as a separate step so that we can share the layer above
#RUN apt-get update && apt-get install -y libgtk2.0-0 libcanberra-gtk-module
RUN apt-get update && apt-get install -y libgtk-3-0 libcanberra-gtk3-module libgtk-3-dev

# Download and install Cinco
RUN wget https://ls5download.cs.tu-dortmund.de/cinco/releases/1.0/cinco-1.0-linux.gtk.x86_64.zip -O /tmp/cinco.zip -q 
RUN echo 'Installing Cinco' && \
    unzip /tmp/cinco.zip -d /opt && \
    rm /tmp/cinco.zip

# Setup users folders and permissions
RUN chmod +x /opt/cinco-1.0/cinco

# Environment variables
RUN useradd -ms /bin/bash developer
USER developer
ENV HOME /home/developer
WORKDIR /home/developer

# Start Cinco-cento
CMD /opt/cinco-1.0/cinco
