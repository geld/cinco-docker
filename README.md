# Cinco on Docker

## Description

Publish the Cinco tool from SCCE.info in an image in DockerHub, with all the required softwares and libraries installed.

## Usage

### Build image locally (for development)

```
# Build Docker image
sudo docker build --rm -t cinco-docker -f Cinco.Dockerfile .
```

### Runs Docker image from DockerHub

```
# Create a folder to share data from the host with the Cinco instance within Docker
mkdir docker-home
chmod a+rwx -R docker-home/

# Forward X
xhost +     # xhost +si:localuser:$USER

# Start Cinco in Docker sharing X and the docker-home folder
sudo docker run -it -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v `pwd`/docker-home:/home/developer/ geld/cinco-docker:latest
```

## Resources

* [Builds Status](https://gitlab.com/geld/cinco-docker/-/jobs)
* [Dockerhub](https://hub.docker.com/repository/docker/geld/cinco-docker)
* [SCCE's Cinco](https://cinco.scce.info/)